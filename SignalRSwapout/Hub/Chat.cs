﻿using SignalR.Hosting.AspNet;
using SignalR.Hubs;
using SignalR.Infrastructure;

public class Chat : Hub
{
	public void Send(string msg)
	{
		// The most basic functionality is signal the addMessage method on all clients
		// and sending the message to be displayed
		Clients.addMessage(msg);

		// Uncomment the following code to send messages back to the specific client
		//Caller.addMessage("This message is specific to the caller.");

		// Uncomment for the additional data demo
		// We're sending this back as an additional message
		// For complex objects, ee can create a typed object and send serialized data back as a JSON array
		//Clients.addMessage(string.Format("Hello!  My name is {0}.", Caller.nickname));

		
		// Comment out all the above code before doing this demo
		//There is a neat example at http://blog.maartenballiauw.be/post/2011/12/06/Using-SignalR-to-broadcast-a-slide-deck.aspx
		//Caller.GroupId = msg;
		//AddToGroup(msg);
		//Clients[msg].addMessage(string.Format("The {0} group!", msg));

		// Use your imagination on the server side, you can query databases

	}

}